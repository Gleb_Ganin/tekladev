﻿using System;
using System.Collections;
using System.Collections.Generic;

using Tekla.Structures.Geometry3d;
using Tekla.Structures.Model.UI;
using Tekla.Structures.Plugins;
using TSM = Tekla.Structures.Model;

namespace FerroBuilding.Truss
{
    public class TrussData
    {
        private static readonly Dictionary<int, Dictionary<ElementTypeEnum, string>> profile_definitions = new Dictionary<int, Dictionary<ElementTypeEnum, string>>();

        private static readonly Dictionary<ElementTypeEnum, Func<string>> custom_profile_definitions = new Dictionary<ElementTypeEnum, Func<string>>();

        internal const double TRUSS_LENGTH = 18000;

        internal const double TRUSS_MIN_HEIGHT = 1180;

        internal const double TRUSS_MAX_HEIGHT = 2080;

        internal const double TRUSS_SPACE = 1500;

        [StructuresField(nameof(LoadingIndex))]
        public int LoadingIndex;

        /// <summary>
        /// Профиль нижнего пояса
        /// </summary>
        [StructuresField(nameof(LowerBraceProfile))]
        public string LowerBraceProfile;

        /// <summary>
        /// Материал нижнего пояса
        /// </summary>
        [StructuresField(nameof(LowerBraceMaterial))]
        public string LowerBraceMaterial;

        /// <summary>
        /// Профиль верхнего пояса
        /// </summary>
        [StructuresField(nameof(UpperBraceProfile))]
        public string UpperBraceProfile;

        /// <summary>
        /// Материал верхнего пояса
        /// </summary>
        [StructuresField(nameof(UpperBraceMaterial))]
        public string UpperBraceMaterial;

        [StructuresField(nameof(FirstBraceProfile))]
        public string FirstBraceProfile;

        [StructuresField(nameof(FirstBraceMaterial))]
        public string FirstBraceMaterial;

        [StructuresField(nameof(SecondBraceProfile))]
        public string SecondBraceProfile;

        [StructuresField(nameof(SecondBraceMaterial))]
        public string SecondBraceMaterial;

        [StructuresField(nameof(ThirdBraceProfile))]
        public string ThirdBraceProfile;

        [StructuresField(nameof(ThirdBraceMaterial))]
        public string ThirdBraceMaterial;

        [StructuresField(nameof(FourthBraceProfile))]
        public string FourthBraceProfile;

        [StructuresField(nameof(FourthBraceMaterial))]
        public string FourthBraceMaterial;

        [StructuresField(nameof(FifthBraceProfile))]
        public string FifthBraceProfile;

        [StructuresField(nameof(FifthBraceMaterial))]
        public string FifthBraceMaterial;

        [StructuresField(nameof(SixthBraceProfile))]
        public string SixthBraceProfile;

        [StructuresField(nameof(SixthBraceMaterial))]
        public string SixthBraceMaterial;

        public string Material;

        static TrussData()
        {
            profile_definitions[0] = new Dictionary<ElementTypeEnum, string>
            {
                { ElementTypeEnum.UpperBrace, "I16B1_20_93" },
                { ElementTypeEnum.LowerBrace, "I10B1_20_93" },
                { ElementTypeEnum.FirstBrace, "PK100X3.0_54157_2010" },
                { ElementTypeEnum.SecondBrace, "PK100X3.0_54157_2010" },
                { ElementTypeEnum.ThirdBrace, "PK50X3.0_54157_2010" },
                { ElementTypeEnum.FourthBrace, "PK50X3.0_54157_2010" },
                { ElementTypeEnum.FifthBrace, "PK50X3.0_54157_2010" },
                { ElementTypeEnum.SixthBrace, "PK50X3.0_54157_2010" },
            };
            profile_definitions[1] = new Dictionary<ElementTypeEnum, string>
            {
                { ElementTypeEnum.UpperBrace, "I18B1_20_93" },
                { ElementTypeEnum.LowerBrace, "I10B1_20_93" },
                { ElementTypeEnum.FirstBrace, "PK120X4.0_54157_2010" },
                { ElementTypeEnum.SecondBrace, "PK120X4.0_54157_2010" },
                { ElementTypeEnum.ThirdBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.FourthBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.FifthBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.SixthBrace, "PK60X3.0_54157_2010" },
            };
            profile_definitions[2] = new Dictionary<ElementTypeEnum, string>
            {
                { ElementTypeEnum.UpperBrace, "I18B1_20_93" },
                { ElementTypeEnum.LowerBrace, "I10B1_20_93" },
                { ElementTypeEnum.FirstBrace, "PK120X4.0_54157_2010" },
                { ElementTypeEnum.SecondBrace, "PK120X4.0_54157_2010" },
                { ElementTypeEnum.ThirdBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.FourthBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.FifthBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.SixthBrace, "PK60X3.0_54157_2010" },
            };
            profile_definitions[3] = new Dictionary<ElementTypeEnum, string>
            {
                { ElementTypeEnum.UpperBrace, "I18B1_20_93" },
                { ElementTypeEnum.LowerBrace, "I12B1_20_93" },
                { ElementTypeEnum.FirstBrace, "PK120X4.0_54157_2010" },
                { ElementTypeEnum.SecondBrace, "PK120X4.0_54157_2010" },
                { ElementTypeEnum.ThirdBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.FourthBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.FifthBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.SixthBrace, "PK60X3.0_54157_2010" },
            };
            profile_definitions[4] = new Dictionary<ElementTypeEnum, string>
            {
                { ElementTypeEnum.UpperBrace, "I18B1_20_93" },
                { ElementTypeEnum.LowerBrace, "I14B1_20_93" },
                { ElementTypeEnum.FirstBrace, "PK120X4.0_54157_2010" },
                { ElementTypeEnum.SecondBrace, "PK120X4.0_54157_2010" },
                { ElementTypeEnum.ThirdBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.FourthBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.FifthBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.SixthBrace, "PK60X3.0_54157_2010" },
            };
            profile_definitions[5] = new Dictionary<ElementTypeEnum, string>
            {
                { ElementTypeEnum.UpperBrace, "I20B1_20_93" },
                { ElementTypeEnum.LowerBrace, "I14B1_20_93" },
                { ElementTypeEnum.FirstBrace, "PK120X4.0_54157_2010" },
                { ElementTypeEnum.SecondBrace, "PK120X4.0_54157_2010" },
                { ElementTypeEnum.ThirdBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.FourthBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.FifthBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.SixthBrace, "PK60X3.0_54157_2010" },
            };
            profile_definitions[6] = new Dictionary<ElementTypeEnum, string>
            {
                { ElementTypeEnum.UpperBrace, "I20B1_20_93" },
                { ElementTypeEnum.LowerBrace, "I16B1_20_93" },
                { ElementTypeEnum.FirstBrace, "PK120X4.0_54157_2010" },
                { ElementTypeEnum.SecondBrace, "PK120X4.0_54157_2010" },
                { ElementTypeEnum.ThirdBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.FourthBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.FifthBrace, "PK60X3.0_54157_2010" },
                { ElementTypeEnum.SixthBrace, "PK60X3.0_54157_2010" },
            };
        }

        public TrussData()
        {
            custom_profile_definitions[ElementTypeEnum.LowerBrace] = () => this.LowerBraceProfile;
            custom_profile_definitions[ElementTypeEnum.UpperBrace] = () => this.UpperBraceProfile;
            custom_profile_definitions[ElementTypeEnum.FirstBrace] = () => this.FirstBraceProfile;
            custom_profile_definitions[ElementTypeEnum.SecondBrace] = () => this.SecondBraceProfile;
            custom_profile_definitions[ElementTypeEnum.ThirdBrace] = () => this.ThirdBraceProfile;
            custom_profile_definitions[ElementTypeEnum.FourthBrace] = () => this.FourthBraceProfile;
            custom_profile_definitions[ElementTypeEnum.FifthBrace] = () => this.FifthBraceProfile;
            custom_profile_definitions[ElementTypeEnum.SixthBrace] = () => this.SixthBraceProfile;
        }

        internal string GetProfile(ElementTypeEnum type)
        {
            var profile = profile_definitions.ContainsKey(this.LoadingIndex) ? profile_definitions[this.LoadingIndex][type] : custom_profile_definitions[type].Invoke();
            return string.IsNullOrEmpty(profile) ? "SQR100" : profile;
        }

        internal string GetMaterial(ElementTypeEnum type) => "";

        internal enum ElementTypeEnum
        {
            LowerBrace = 0,

            UpperBrace = 7,

            FirstBrace = 1,

            SecondBrace = 2,

            ThirdBrace = 3,

            FourthBrace = 4,

            FifthBrace = 5,

            SixthBrace = 6,
        }
    }

    [Plugin("TrussPlugin")]
    [PluginUserInterface("FerroBuilding.Truss.TrussPluginForm")]
    public class TrussPlugin : PluginBase
    {
        private const int BRACE_POINTS_COUNT = 7;

        private readonly Vector[] brace_point_vertical_vectors = new Vector[BRACE_POINTS_COUNT];
        private readonly Vector[] brace_point_horizontal_vectors = new Vector[BRACE_POINTS_COUNT];

        private bool brace_vectors_defined;

        private readonly TrussData data;

        public TrussPlugin(TrussData data)
        {
            TSM.Model model = new TSM.Model();
            this.data = data;
        }

        public override List<InputDefinition> DefineInput()
        {
            Picker picker = new Picker();
            List<InputDefinition> input_definition = new List<InputDefinition>();
            var picked_points = picker.PickPoints(Picker.PickPointEnum.PICK_TWO_POINTS, "Укажите начальную и конечную точки фермы.");
            input_definition.Add(new InputDefinition(picked_points));
            return input_definition;
        }

        public override bool Run(List<InputDefinition> Input)
        {
            try
            {
                this.define_support_points(Input, out Point start_point, out Point end_point);

                this.create_lower_brace(start_point, end_point, out Point center_point);

                this.create_upper_brace(start_point, end_point, center_point);

                if (!this.brace_vectors_defined) this.brace_vectors_defined = this.define_brace_vectors(new Vector(end_point - start_point));

                this.create_other_braces(start_point, end_point);
            }
            catch (Exception ex)
            {
            }

            return true;
        }

        private void define_support_points(List<InputDefinition> input, out Point start_point, out Point end_point)
        {
            var points = input[0].GetInput() as ArrayList;
            start_point = points[0] as Point;
            end_point = points[1] as Point;

            var vector = end_point - start_point;
            var length_vector = new Vector(vector);
            length_vector.Normalize(TrussData.TRUSS_LENGTH);
            end_point = start_point + length_vector;
        }

        private bool define_brace_vectors(Vector length_vector)
        {
            try
            {
                var angle = Math.Atan((TrussData.TRUSS_MAX_HEIGHT - TrussData.TRUSS_MIN_HEIGHT) / (TrussData.TRUSS_LENGTH / 2));
                this.brace_point_vertical_vectors[0] = new Vector(0, 0, TrussData.TRUSS_MIN_HEIGHT);
                double current_space = 0;
                for (int i = 0; i < BRACE_POINTS_COUNT; i++)
                {
                    if (i % 2 > 0) this.brace_point_vertical_vectors[i] = new Vector();
                    else
                    {
                        double delta_z = current_space * Math.Tan(angle);
                        this.brace_point_vertical_vectors[i] = new Vector() { Z = TrussData.TRUSS_MIN_HEIGHT + delta_z };
                    }
                    this.brace_point_horizontal_vectors[i] = new Vector(length_vector);
                    this.brace_point_horizontal_vectors[i].Normalize(current_space);

                    current_space += TrussData.TRUSS_SPACE;
                }

                return true;
            }
            catch (Exception ex)
            {


                return false;
            }
        }

        private void create_lower_brace(Point start_point, Point end_point, out Point center_point)
        {
            center_point = this.get_center_point(start_point, end_point);
            var profile = this.data.GetProfile(TrussData.ElementTypeEnum.LowerBrace);
            this.create_beam(start_point, center_point, profile, this.data.Material);
            this.create_beam(end_point, center_point, profile, this.data.Material);
        }

        private void create_upper_brace(Point start_point, Point end_point, Point center_point)
        {
            var min_height_vector = new Vector(0, 0, TrussData.TRUSS_MIN_HEIGHT);
            var max_height_vector = new Vector(0, 0, TrussData.TRUSS_MAX_HEIGHT);
            var profile = this.data.GetProfile(TrussData.ElementTypeEnum.UpperBrace);
            this.create_beam(start_point + min_height_vector, center_point + max_height_vector, profile, this.data.Material);
            this.create_beam(end_point + min_height_vector, center_point + max_height_vector, profile, this.data.Material);
        }

        private void create_other_braces(Point start_point, Point end_point)
        {
            for (int i = 1; i < BRACE_POINTS_COUNT; i++)
            {
                var first_point_start = start_point + this.brace_point_vertical_vectors[i - 1] + this.brace_point_horizontal_vectors[i - 1];
                var second_point_start = start_point + this.brace_point_vertical_vectors[i] + this.brace_point_horizontal_vectors[i];
                var first_point_end = end_point + this.brace_point_vertical_vectors[i - 1] - this.brace_point_horizontal_vectors[i - 1];
                var second_point_end = end_point + this.brace_point_vertical_vectors[i] - this.brace_point_horizontal_vectors[i];
                var profile = this.data.GetProfile((TrussData.ElementTypeEnum)i);
                var material = this.data.GetMaterial((TrussData.ElementTypeEnum)i);
                this.create_beam(first_point_start, second_point_start, profile, material);
                this.create_beam(first_point_end, second_point_end, profile, material);
            }
        }

        private Point get_center_point(Point first_point, Point second_point) => new Point
        {
            X = (first_point.X + second_point.X) / 2,
            Y = (first_point.Y + second_point.Y) / 2,
            Z = (first_point.Z + second_point.Z) / 2,
        };

        private void create_beam(Point Point1, Point Point2, string profile_string, string material_string)
        {
            TSM.Beam beam = new TSM.Beam(Point1, Point2);
            beam.Profile.ProfileString = profile_string;
            beam.Material.MaterialString = "Steel_Undefined";
            beam.Position.Depth = TSM.Position.DepthEnum.MIDDLE;
            beam.Position.Plane = TSM.Position.PlaneEnum.MIDDLE;
            beam.PartNumber.Prefix = null;
            beam.AssemblyNumber.Prefix = null;
            beam.Class = "2";
            beam.Insert();
        }
    }
}
