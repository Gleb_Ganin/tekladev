﻿namespace FerroBuilding.Truss
{
    partial class TrussPluginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okApplyModifyGetOnOffCancel1 = new Tekla.Structures.Dialog.UIControls.OkApplyModifyGetOnOffCancel();
            this.saveLoad1 = new Tekla.Structures.Dialog.UIControls.SaveLoad();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tBLowerBraceMaterial = new System.Windows.Forms.TextBox();
            this.tBLowerBraceProfile = new System.Windows.Forms.TextBox();
            this.profileCatalog1 = new Tekla.Structures.Dialog.UIControls.ProfileCatalog();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.materialCatalog1 = new Tekla.Structures.Dialog.UIControls.MaterialCatalog();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.tBUpperBraceProfile = new System.Windows.Forms.TextBox();
            this.profileCatalog2 = new Tekla.Structures.Dialog.UIControls.ProfileCatalog();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.tBUpperBraceMaterial = new System.Windows.Forms.TextBox();
            this.materialCatalog2 = new Tekla.Structures.Dialog.UIControls.MaterialCatalog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tBFirstBraceMaterial = new System.Windows.Forms.TextBox();
            this.tBFirstBraceProfile = new System.Windows.Forms.TextBox();
            this.profileCatalog3 = new Tekla.Structures.Dialog.UIControls.ProfileCatalog();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.materialCatalog3 = new Tekla.Structures.Dialog.UIControls.MaterialCatalog();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.tBSecondBraceProfile = new System.Windows.Forms.TextBox();
            this.profileCatalog4 = new Tekla.Structures.Dialog.UIControls.ProfileCatalog();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.tBSecondBraceMaterial = new System.Windows.Forms.TextBox();
            this.materialCatalog4 = new Tekla.Structures.Dialog.UIControls.MaterialCatalog();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tBThirdBraceMaterial = new System.Windows.Forms.TextBox();
            this.tBThirdBraceProfile = new System.Windows.Forms.TextBox();
            this.profileCatalog5 = new Tekla.Structures.Dialog.UIControls.ProfileCatalog();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.materialCatalog5 = new Tekla.Structures.Dialog.UIControls.MaterialCatalog();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.tBFourthBraceProfile = new System.Windows.Forms.TextBox();
            this.profileCatalog6 = new Tekla.Structures.Dialog.UIControls.ProfileCatalog();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.tBFourthBraceMaterial = new System.Windows.Forms.TextBox();
            this.materialCatalog6 = new Tekla.Structures.Dialog.UIControls.MaterialCatalog();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tBFifthBraceMaterial = new System.Windows.Forms.TextBox();
            this.tBFifthBraceProfile = new System.Windows.Forms.TextBox();
            this.profileCatalog7 = new Tekla.Structures.Dialog.UIControls.ProfileCatalog();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.materialCatalog7 = new Tekla.Structures.Dialog.UIControls.MaterialCatalog();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.tBSixthBraceProfile = new System.Windows.Forms.TextBox();
            this.profileCatalog8 = new Tekla.Structures.Dialog.UIControls.ProfileCatalog();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.tBSixthBraceMaterial = new System.Windows.Forms.TextBox();
            this.materialCatalog8 = new Tekla.Structures.Dialog.UIControls.MaterialCatalog();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // okApplyModifyGetOnOffCancel1
            // 
            this.structuresExtender.SetAttributeName(this.okApplyModifyGetOnOffCancel1, null);
            this.structuresExtender.SetAttributeTypeName(this.okApplyModifyGetOnOffCancel1, null);
            this.structuresExtender.SetBindPropertyName(this.okApplyModifyGetOnOffCancel1, null);
            this.okApplyModifyGetOnOffCancel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.okApplyModifyGetOnOffCancel1.Location = new System.Drawing.Point(0, 229);
            this.okApplyModifyGetOnOffCancel1.Name = "okApplyModifyGetOnOffCancel1";
            this.okApplyModifyGetOnOffCancel1.Size = new System.Drawing.Size(538, 29);
            this.okApplyModifyGetOnOffCancel1.TabIndex = 0;
            this.okApplyModifyGetOnOffCancel1.OkClicked += new System.EventHandler(this.okApplyModifyGetOnOffCancel1_OkClicked);
            this.okApplyModifyGetOnOffCancel1.ApplyClicked += new System.EventHandler(this.okApplyModifyGetOnOffCancel1_ApplyClicked);
            this.okApplyModifyGetOnOffCancel1.ModifyClicked += new System.EventHandler(this.okApplyModifyGetOnOffCancel1_ModifyClicked);
            this.okApplyModifyGetOnOffCancel1.GetClicked += new System.EventHandler(this.okApplyModifyGetOnOffCancel1_GetClicked);
            this.okApplyModifyGetOnOffCancel1.OnOffClicked += new System.EventHandler(this.okApplyModifyGetOnOffCancel1_OnOffClicked);
            this.okApplyModifyGetOnOffCancel1.CancelClicked += new System.EventHandler(this.okApplyModifyGetOnOffCancel1_CancelClicked);
            // 
            // saveLoad1
            // 
            this.structuresExtender.SetAttributeName(this.saveLoad1, null);
            this.structuresExtender.SetAttributeTypeName(this.saveLoad1, null);
            this.saveLoad1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.structuresExtender.SetBindPropertyName(this.saveLoad1, null);
            this.saveLoad1.Dock = System.Windows.Forms.DockStyle.Top;
            this.saveLoad1.HelpFileType = Tekla.Structures.Dialog.UIControls.SaveLoad.HelpFileTypeEnum.General;
            this.saveLoad1.HelpKeyword = "";
            this.saveLoad1.HelpUrl = "";
            this.saveLoad1.Location = new System.Drawing.Point(0, 0);
            this.saveLoad1.Name = "saveLoad1";
            this.saveLoad1.SaveAsText = "";
            this.saveLoad1.Size = new System.Drawing.Size(538, 43);
            this.saveLoad1.TabIndex = 1;
            this.saveLoad1.UserDefinedHelpFilePath = null;
            // 
            // tableLayoutPanel1
            // 
            this.structuresExtender.SetAttributeName(this.tableLayoutPanel1, null);
            this.structuresExtender.SetAttributeTypeName(this.tableLayoutPanel1, null);
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.structuresExtender.SetBindPropertyName(this.tableLayoutPanel1, null);
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.tBLowerBraceMaterial, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tBLowerBraceProfile, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.profileCatalog1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBox6, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.materialCatalog1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.checkBox5, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBox7, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tBUpperBraceProfile, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.profileCatalog2, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.checkBox8, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tBUpperBraceMaterial, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.materialCatalog2, 2, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 6);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(364, 112);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // tBLowerBraceMaterial
            // 
            this.tBLowerBraceMaterial.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.tBLowerBraceMaterial, "LowerBraceMaterial");
            this.structuresExtender.SetAttributeTypeName(this.tBLowerBraceMaterial, "String");
            this.structuresExtender.SetBindPropertyName(this.tBLowerBraceMaterial, "Text");
            this.tBLowerBraceMaterial.Enabled = false;
            this.tBLowerBraceMaterial.Location = new System.Drawing.Point(167, 32);
            this.tBLowerBraceMaterial.Name = "tBLowerBraceMaterial";
            this.tBLowerBraceMaterial.Size = new System.Drawing.Size(100, 20);
            this.tBLowerBraceMaterial.TabIndex = 9;
            // 
            // tBLowerBraceProfile
            // 
            this.tBLowerBraceProfile.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.tBLowerBraceProfile, "LowerBraceProfile");
            this.structuresExtender.SetAttributeTypeName(this.tBLowerBraceProfile, "String");
            this.structuresExtender.SetBindPropertyName(this.tBLowerBraceProfile, "Text");
            this.tBLowerBraceProfile.Enabled = false;
            this.tBLowerBraceProfile.Location = new System.Drawing.Point(167, 4);
            this.tBLowerBraceProfile.Name = "tBLowerBraceProfile";
            this.tBLowerBraceProfile.Size = new System.Drawing.Size(100, 20);
            this.tBLowerBraceProfile.TabIndex = 9;
            // 
            // profileCatalog1
            // 
            this.profileCatalog1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.profileCatalog1, null);
            this.structuresExtender.SetAttributeTypeName(this.profileCatalog1, null);
            this.profileCatalog1.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.profileCatalog1, null);
            this.profileCatalog1.Location = new System.Drawing.Point(273, 3);
            this.profileCatalog1.Name = "profileCatalog1";
            this.profileCatalog1.SelectedProfile = "";
            this.profileCatalog1.Size = new System.Drawing.Size(88, 22);
            this.profileCatalog1.TabIndex = 8;
            this.profileCatalog1.SelectClicked += new System.EventHandler(this.profileCatalog1_SelectClicked);
            this.profileCatalog1.SelectionDone += new System.EventHandler(this.profileCatalog1_SelectionDone);
            // 
            // checkBox6
            // 
            this.checkBox6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.checkBox6, "LowerBraceMaterial");
            this.structuresExtender.SetAttributeTypeName(this.checkBox6, null);
            this.checkBox6.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox6, null);
            this.structuresExtender.SetIsFilter(this.checkBox6, true);
            this.checkBox6.Location = new System.Drawing.Point(3, 33);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(155, 17);
            this.checkBox6.TabIndex = 6;
            this.checkBox6.Text = "Материал нижнего пояса";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // materialCatalog1
            // 
            this.structuresExtender.SetAttributeName(this.materialCatalog1, null);
            this.structuresExtender.SetAttributeTypeName(this.materialCatalog1, null);
            this.materialCatalog1.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.materialCatalog1, null);
            this.materialCatalog1.Location = new System.Drawing.Point(273, 31);
            this.materialCatalog1.Name = "materialCatalog1";
            this.materialCatalog1.SelectedMaterial = "";
            this.materialCatalog1.Size = new System.Drawing.Size(88, 22);
            this.materialCatalog1.TabIndex = 10;
            this.materialCatalog1.SelectClicked += new System.EventHandler(this.materialCatalog1_SelectClicked);
            this.materialCatalog1.SelectionDone += new System.EventHandler(this.materialCatalog1_SelectionDone);
            // 
            // checkBox5
            // 
            this.checkBox5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.checkBox5, "LowerBraceProfile");
            this.structuresExtender.SetAttributeTypeName(this.checkBox5, null);
            this.checkBox5.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox5, null);
            this.structuresExtender.SetIsFilter(this.checkBox5, true);
            this.checkBox5.Location = new System.Drawing.Point(3, 5);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(151, 17);
            this.checkBox5.TabIndex = 6;
            this.checkBox5.Text = "Профиль нижнего пояса";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this.checkBox7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.checkBox7, "UpperBraceProfile");
            this.structuresExtender.SetAttributeTypeName(this.checkBox7, null);
            this.checkBox7.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox7, null);
            this.structuresExtender.SetIsFilter(this.checkBox7, true);
            this.checkBox7.Location = new System.Drawing.Point(3, 61);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(154, 17);
            this.checkBox7.TabIndex = 6;
            this.checkBox7.Text = "Профиль верхнего пояса";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // tBUpperBraceProfile
            // 
            this.tBUpperBraceProfile.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.tBUpperBraceProfile, "UpperBraceProfile");
            this.structuresExtender.SetAttributeTypeName(this.tBUpperBraceProfile, "String");
            this.structuresExtender.SetBindPropertyName(this.tBUpperBraceProfile, "Text");
            this.tBUpperBraceProfile.Enabled = false;
            this.tBUpperBraceProfile.Location = new System.Drawing.Point(167, 60);
            this.tBUpperBraceProfile.Name = "tBUpperBraceProfile";
            this.tBUpperBraceProfile.Size = new System.Drawing.Size(100, 20);
            this.tBUpperBraceProfile.TabIndex = 9;
            // 
            // profileCatalog2
            // 
            this.profileCatalog2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.profileCatalog2, null);
            this.structuresExtender.SetAttributeTypeName(this.profileCatalog2, null);
            this.profileCatalog2.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.profileCatalog2, null);
            this.profileCatalog2.Location = new System.Drawing.Point(273, 59);
            this.profileCatalog2.Name = "profileCatalog2";
            this.profileCatalog2.SelectedProfile = "";
            this.profileCatalog2.Size = new System.Drawing.Size(88, 22);
            this.profileCatalog2.TabIndex = 8;
            this.profileCatalog2.SelectClicked += new System.EventHandler(this.profileCatalog2_SelectClicked);
            this.profileCatalog2.SelectionDone += new System.EventHandler(this.profileCatalog2_SelectionDone);
            // 
            // checkBox8
            // 
            this.checkBox8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.checkBox8, "UpperBraceMaterial");
            this.structuresExtender.SetAttributeTypeName(this.checkBox8, null);
            this.checkBox8.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox8, null);
            this.structuresExtender.SetIsFilter(this.checkBox8, true);
            this.checkBox8.Location = new System.Drawing.Point(3, 89);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(158, 17);
            this.checkBox8.TabIndex = 6;
            this.checkBox8.Text = "Материал верхнего пояса";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // tBUpperBraceMaterial
            // 
            this.tBUpperBraceMaterial.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.tBUpperBraceMaterial, "UpperBraceMaterial");
            this.structuresExtender.SetAttributeTypeName(this.tBUpperBraceMaterial, "String");
            this.structuresExtender.SetBindPropertyName(this.tBUpperBraceMaterial, "Text");
            this.tBUpperBraceMaterial.Enabled = false;
            this.tBUpperBraceMaterial.Location = new System.Drawing.Point(167, 88);
            this.tBUpperBraceMaterial.Name = "tBUpperBraceMaterial";
            this.tBUpperBraceMaterial.Size = new System.Drawing.Size(100, 20);
            this.tBUpperBraceMaterial.TabIndex = 9;
            // 
            // materialCatalog2
            // 
            this.structuresExtender.SetAttributeName(this.materialCatalog2, null);
            this.structuresExtender.SetAttributeTypeName(this.materialCatalog2, null);
            this.materialCatalog2.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.materialCatalog2, null);
            this.materialCatalog2.Location = new System.Drawing.Point(273, 87);
            this.materialCatalog2.Name = "materialCatalog2";
            this.materialCatalog2.SelectedMaterial = "";
            this.materialCatalog2.Size = new System.Drawing.Size(88, 22);
            this.materialCatalog2.TabIndex = 10;
            this.materialCatalog2.SelectClicked += new System.EventHandler(this.materialCatalog2_SelectClicked);
            this.materialCatalog2.SelectionDone += new System.EventHandler(this.materialCatalog2_SelectionDone);
            // 
            // tabControl1
            // 
            this.structuresExtender.SetAttributeName(this.tabControl1, null);
            this.structuresExtender.SetAttributeTypeName(this.tabControl1, null);
            this.structuresExtender.SetBindPropertyName(this.tabControl1, null);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(7, 80);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(531, 149);
            this.tabControl1.TabIndex = 11;
            // 
            // tabPage1
            // 
            this.structuresExtender.SetAttributeName(this.tabPage1, null);
            this.structuresExtender.SetAttributeTypeName(this.tabPage1, null);
            this.structuresExtender.SetBindPropertyName(this.tabPage1, null);
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(523, 123);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Свойства поясов";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.structuresExtender.SetAttributeName(this.tabPage2, null);
            this.structuresExtender.SetAttributeTypeName(this.tabPage2, null);
            this.structuresExtender.SetBindPropertyName(this.tabPage2, null);
            this.tabPage2.Controls.Add(this.tableLayoutPanel2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(523, 123);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Свойства раскосов 1 - 2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.structuresExtender.SetAttributeName(this.tableLayoutPanel2, null);
            this.structuresExtender.SetAttributeTypeName(this.tableLayoutPanel2, null);
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.structuresExtender.SetBindPropertyName(this.tableLayoutPanel2, null);
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.tBFirstBraceMaterial, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.tBFirstBraceProfile, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.profileCatalog3, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.checkBox1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.materialCatalog3, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.checkBox2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.checkBox3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tBSecondBraceProfile, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.profileCatalog4, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.checkBox4, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.tBSecondBraceMaterial, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.materialCatalog4, 2, 3);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 6);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(336, 112);
            this.tableLayoutPanel2.TabIndex = 11;
            // 
            // tBFirstBraceMaterial
            // 
            this.tBFirstBraceMaterial.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.tBFirstBraceMaterial, "FirstBraceMaterial");
            this.structuresExtender.SetAttributeTypeName(this.tBFirstBraceMaterial, "String");
            this.structuresExtender.SetBindPropertyName(this.tBFirstBraceMaterial, "Text");
            this.tBFirstBraceMaterial.Enabled = false;
            this.tBFirstBraceMaterial.Location = new System.Drawing.Point(139, 32);
            this.tBFirstBraceMaterial.Name = "tBFirstBraceMaterial";
            this.tBFirstBraceMaterial.Size = new System.Drawing.Size(100, 20);
            this.tBFirstBraceMaterial.TabIndex = 9;
            // 
            // tBFirstBraceProfile
            // 
            this.tBFirstBraceProfile.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.tBFirstBraceProfile, "FirstBraceProfile");
            this.structuresExtender.SetAttributeTypeName(this.tBFirstBraceProfile, "String");
            this.structuresExtender.SetBindPropertyName(this.tBFirstBraceProfile, "Text");
            this.tBFirstBraceProfile.Enabled = false;
            this.tBFirstBraceProfile.Location = new System.Drawing.Point(139, 4);
            this.tBFirstBraceProfile.Name = "tBFirstBraceProfile";
            this.tBFirstBraceProfile.Size = new System.Drawing.Size(100, 20);
            this.tBFirstBraceProfile.TabIndex = 9;
            // 
            // profileCatalog3
            // 
            this.profileCatalog3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.profileCatalog3, null);
            this.structuresExtender.SetAttributeTypeName(this.profileCatalog3, null);
            this.profileCatalog3.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.profileCatalog3, null);
            this.profileCatalog3.Location = new System.Drawing.Point(245, 3);
            this.profileCatalog3.Name = "profileCatalog3";
            this.profileCatalog3.SelectedProfile = "";
            this.profileCatalog3.Size = new System.Drawing.Size(88, 22);
            this.profileCatalog3.TabIndex = 8;
            this.profileCatalog3.SelectClicked += new System.EventHandler(this.profileCatalog3_SelectClicked);
            this.profileCatalog3.SelectionDone += new System.EventHandler(this.profileCatalog3_SelectionDone);
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.checkBox1, "FirstBraceMaterial");
            this.structuresExtender.SetAttributeTypeName(this.checkBox1, null);
            this.checkBox1.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox1, null);
            this.structuresExtender.SetIsFilter(this.checkBox1, true);
            this.checkBox1.Location = new System.Drawing.Point(3, 33);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(130, 17);
            this.checkBox1.TabIndex = 6;
            this.checkBox1.Text = "Материал раскоса 1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // materialCatalog3
            // 
            this.structuresExtender.SetAttributeName(this.materialCatalog3, null);
            this.structuresExtender.SetAttributeTypeName(this.materialCatalog3, null);
            this.materialCatalog3.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.materialCatalog3, null);
            this.materialCatalog3.Location = new System.Drawing.Point(245, 31);
            this.materialCatalog3.Name = "materialCatalog3";
            this.materialCatalog3.SelectedMaterial = "";
            this.materialCatalog3.Size = new System.Drawing.Size(88, 22);
            this.materialCatalog3.TabIndex = 10;
            this.materialCatalog3.SelectClicked += new System.EventHandler(this.materialCatalog3_SelectClicked);
            this.materialCatalog3.SelectionDone += new System.EventHandler(this.materialCatalog3_SelectionDone);
            // 
            // checkBox2
            // 
            this.checkBox2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.checkBox2, "FirstBraceProfile");
            this.structuresExtender.SetAttributeTypeName(this.checkBox2, null);
            this.checkBox2.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox2, null);
            this.structuresExtender.SetIsFilter(this.checkBox2, true);
            this.checkBox2.Location = new System.Drawing.Point(3, 5);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(126, 17);
            this.checkBox2.TabIndex = 6;
            this.checkBox2.Text = "Профиль раскоса 1";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.checkBox3, "SecondBraceProfile");
            this.structuresExtender.SetAttributeTypeName(this.checkBox3, null);
            this.checkBox3.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox3, null);
            this.structuresExtender.SetIsFilter(this.checkBox3, true);
            this.checkBox3.Location = new System.Drawing.Point(3, 61);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(126, 17);
            this.checkBox3.TabIndex = 6;
            this.checkBox3.Text = "Профиль раскоса 2";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // tBSecondBraceProfile
            // 
            this.tBSecondBraceProfile.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.tBSecondBraceProfile, "SecondBraceProfile");
            this.structuresExtender.SetAttributeTypeName(this.tBSecondBraceProfile, "String");
            this.structuresExtender.SetBindPropertyName(this.tBSecondBraceProfile, "Text");
            this.tBSecondBraceProfile.Enabled = false;
            this.tBSecondBraceProfile.Location = new System.Drawing.Point(139, 60);
            this.tBSecondBraceProfile.Name = "tBSecondBraceProfile";
            this.tBSecondBraceProfile.Size = new System.Drawing.Size(100, 20);
            this.tBSecondBraceProfile.TabIndex = 9;
            // 
            // profileCatalog4
            // 
            this.profileCatalog4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.profileCatalog4, null);
            this.structuresExtender.SetAttributeTypeName(this.profileCatalog4, null);
            this.profileCatalog4.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.profileCatalog4, null);
            this.profileCatalog4.Location = new System.Drawing.Point(245, 59);
            this.profileCatalog4.Name = "profileCatalog4";
            this.profileCatalog4.SelectedProfile = "";
            this.profileCatalog4.Size = new System.Drawing.Size(88, 22);
            this.profileCatalog4.TabIndex = 8;
            this.profileCatalog4.SelectClicked += new System.EventHandler(this.profileCatalog4_SelectClicked);
            this.profileCatalog4.SelectionDone += new System.EventHandler(this.profileCatalog4_SelectionDone);
            // 
            // checkBox4
            // 
            this.checkBox4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.checkBox4, "SecondBraceMaterial");
            this.structuresExtender.SetAttributeTypeName(this.checkBox4, null);
            this.checkBox4.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox4, null);
            this.structuresExtender.SetIsFilter(this.checkBox4, true);
            this.checkBox4.Location = new System.Drawing.Point(3, 89);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(130, 17);
            this.checkBox4.TabIndex = 6;
            this.checkBox4.Text = "Материал раскоса 2";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // tBSecondBraceMaterial
            // 
            this.tBSecondBraceMaterial.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.tBSecondBraceMaterial, "SecondBraceMaterial");
            this.structuresExtender.SetAttributeTypeName(this.tBSecondBraceMaterial, "String");
            this.structuresExtender.SetBindPropertyName(this.tBSecondBraceMaterial, "Text");
            this.tBSecondBraceMaterial.Enabled = false;
            this.tBSecondBraceMaterial.Location = new System.Drawing.Point(139, 88);
            this.tBSecondBraceMaterial.Name = "tBSecondBraceMaterial";
            this.tBSecondBraceMaterial.Size = new System.Drawing.Size(100, 20);
            this.tBSecondBraceMaterial.TabIndex = 9;
            // 
            // materialCatalog4
            // 
            this.structuresExtender.SetAttributeName(this.materialCatalog4, null);
            this.structuresExtender.SetAttributeTypeName(this.materialCatalog4, null);
            this.materialCatalog4.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.materialCatalog4, null);
            this.materialCatalog4.Location = new System.Drawing.Point(245, 87);
            this.materialCatalog4.Name = "materialCatalog4";
            this.materialCatalog4.SelectedMaterial = "";
            this.materialCatalog4.Size = new System.Drawing.Size(88, 22);
            this.materialCatalog4.TabIndex = 10;
            this.materialCatalog4.SelectClicked += new System.EventHandler(this.materialCatalog4_SelectClicked);
            this.materialCatalog4.SelectionDone += new System.EventHandler(this.materialCatalog4_SelectionDone);
            // 
            // tabPage3
            // 
            this.structuresExtender.SetAttributeName(this.tabPage3, null);
            this.structuresExtender.SetAttributeTypeName(this.tabPage3, null);
            this.structuresExtender.SetBindPropertyName(this.tabPage3, null);
            this.tabPage3.Controls.Add(this.tableLayoutPanel3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(523, 123);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Свойства раскосов 3 - 4";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.structuresExtender.SetAttributeName(this.tableLayoutPanel3, null);
            this.structuresExtender.SetAttributeTypeName(this.tableLayoutPanel3, null);
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.structuresExtender.SetBindPropertyName(this.tableLayoutPanel3, null);
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.Controls.Add(this.tBThirdBraceMaterial, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.tBThirdBraceProfile, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.profileCatalog5, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.checkBox9, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.materialCatalog5, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.checkBox10, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.checkBox11, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.tBFourthBraceProfile, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.profileCatalog6, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.checkBox12, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.tBFourthBraceMaterial, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.materialCatalog6, 2, 3);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(6, 6);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(336, 112);
            this.tableLayoutPanel3.TabIndex = 12;
            // 
            // tBThirdBraceMaterial
            // 
            this.tBThirdBraceMaterial.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.tBThirdBraceMaterial, "ThirdBraceMaterial");
            this.structuresExtender.SetAttributeTypeName(this.tBThirdBraceMaterial, "String");
            this.structuresExtender.SetBindPropertyName(this.tBThirdBraceMaterial, "Text");
            this.tBThirdBraceMaterial.Enabled = false;
            this.tBThirdBraceMaterial.Location = new System.Drawing.Point(139, 32);
            this.tBThirdBraceMaterial.Name = "tBThirdBraceMaterial";
            this.tBThirdBraceMaterial.Size = new System.Drawing.Size(100, 20);
            this.tBThirdBraceMaterial.TabIndex = 9;
            // 
            // tBThirdBraceProfile
            // 
            this.tBThirdBraceProfile.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.tBThirdBraceProfile, "ThirdBraceProfile");
            this.structuresExtender.SetAttributeTypeName(this.tBThirdBraceProfile, "String");
            this.structuresExtender.SetBindPropertyName(this.tBThirdBraceProfile, "Text");
            this.tBThirdBraceProfile.Enabled = false;
            this.tBThirdBraceProfile.Location = new System.Drawing.Point(139, 4);
            this.tBThirdBraceProfile.Name = "tBThirdBraceProfile";
            this.tBThirdBraceProfile.Size = new System.Drawing.Size(100, 20);
            this.tBThirdBraceProfile.TabIndex = 9;
            // 
            // profileCatalog5
            // 
            this.profileCatalog5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.profileCatalog5, null);
            this.structuresExtender.SetAttributeTypeName(this.profileCatalog5, null);
            this.profileCatalog5.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.profileCatalog5, null);
            this.profileCatalog5.Location = new System.Drawing.Point(245, 3);
            this.profileCatalog5.Name = "profileCatalog5";
            this.profileCatalog5.SelectedProfile = "";
            this.profileCatalog5.Size = new System.Drawing.Size(88, 22);
            this.profileCatalog5.TabIndex = 8;
            this.profileCatalog5.SelectClicked += new System.EventHandler(this.profileCatalog5_SelectClicked);
            this.profileCatalog5.SelectionDone += new System.EventHandler(this.profileCatalog5_SelectionDone);
            // 
            // checkBox9
            // 
            this.checkBox9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.checkBox9, "ThirdBraceMaterial");
            this.structuresExtender.SetAttributeTypeName(this.checkBox9, null);
            this.checkBox9.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox9, null);
            this.structuresExtender.SetIsFilter(this.checkBox9, true);
            this.checkBox9.Location = new System.Drawing.Point(3, 33);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(130, 17);
            this.checkBox9.TabIndex = 6;
            this.checkBox9.Text = "Материал раскоса 3";
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // materialCatalog5
            // 
            this.structuresExtender.SetAttributeName(this.materialCatalog5, null);
            this.structuresExtender.SetAttributeTypeName(this.materialCatalog5, null);
            this.materialCatalog5.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.materialCatalog5, null);
            this.materialCatalog5.Location = new System.Drawing.Point(245, 31);
            this.materialCatalog5.Name = "materialCatalog5";
            this.materialCatalog5.SelectedMaterial = "";
            this.materialCatalog5.Size = new System.Drawing.Size(88, 22);
            this.materialCatalog5.TabIndex = 10;
            this.materialCatalog5.SelectClicked += new System.EventHandler(this.materialCatalog5_SelectClicked);
            this.materialCatalog5.SelectionDone += new System.EventHandler(this.materialCatalog5_SelectionDone);
            // 
            // checkBox10
            // 
            this.checkBox10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.checkBox10, "ThirdBraceProfile");
            this.structuresExtender.SetAttributeTypeName(this.checkBox10, null);
            this.checkBox10.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox10, null);
            this.structuresExtender.SetIsFilter(this.checkBox10, true);
            this.checkBox10.Location = new System.Drawing.Point(3, 5);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(126, 17);
            this.checkBox10.TabIndex = 6;
            this.checkBox10.Text = "Профиль раскоса 3";
            this.checkBox10.UseVisualStyleBackColor = true;
            // 
            // checkBox11
            // 
            this.checkBox11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.checkBox11, "FourthBraceProfile");
            this.structuresExtender.SetAttributeTypeName(this.checkBox11, null);
            this.checkBox11.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox11, null);
            this.structuresExtender.SetIsFilter(this.checkBox11, true);
            this.checkBox11.Location = new System.Drawing.Point(3, 61);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(126, 17);
            this.checkBox11.TabIndex = 6;
            this.checkBox11.Text = "Профиль раскоса 4";
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // tBFourthBraceProfile
            // 
            this.tBFourthBraceProfile.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.tBFourthBraceProfile, "FourthBraceProfile");
            this.structuresExtender.SetAttributeTypeName(this.tBFourthBraceProfile, "String");
            this.structuresExtender.SetBindPropertyName(this.tBFourthBraceProfile, "Text");
            this.tBFourthBraceProfile.Enabled = false;
            this.tBFourthBraceProfile.Location = new System.Drawing.Point(139, 60);
            this.tBFourthBraceProfile.Name = "tBFourthBraceProfile";
            this.tBFourthBraceProfile.Size = new System.Drawing.Size(100, 20);
            this.tBFourthBraceProfile.TabIndex = 9;
            // 
            // profileCatalog6
            // 
            this.profileCatalog6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.profileCatalog6, null);
            this.structuresExtender.SetAttributeTypeName(this.profileCatalog6, null);
            this.profileCatalog6.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.profileCatalog6, null);
            this.profileCatalog6.Location = new System.Drawing.Point(245, 59);
            this.profileCatalog6.Name = "profileCatalog6";
            this.profileCatalog6.SelectedProfile = "";
            this.profileCatalog6.Size = new System.Drawing.Size(88, 22);
            this.profileCatalog6.TabIndex = 8;
            this.profileCatalog6.SelectClicked += new System.EventHandler(this.profileCatalog6_SelectClicked);
            this.profileCatalog6.SelectionDone += new System.EventHandler(this.profileCatalog6_SelectionDone);
            // 
            // checkBox12
            // 
            this.checkBox12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.checkBox12, "FourthBraceMaterial");
            this.structuresExtender.SetAttributeTypeName(this.checkBox12, null);
            this.checkBox12.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox12, null);
            this.structuresExtender.SetIsFilter(this.checkBox12, true);
            this.checkBox12.Location = new System.Drawing.Point(3, 89);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(130, 17);
            this.checkBox12.TabIndex = 6;
            this.checkBox12.Text = "Материал раскоса 4";
            this.checkBox12.UseVisualStyleBackColor = true;
            // 
            // tBFourthBraceMaterial
            // 
            this.tBFourthBraceMaterial.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.tBFourthBraceMaterial, "FourthBraceMaterial");
            this.structuresExtender.SetAttributeTypeName(this.tBFourthBraceMaterial, "String");
            this.structuresExtender.SetBindPropertyName(this.tBFourthBraceMaterial, "Text");
            this.tBFourthBraceMaterial.Enabled = false;
            this.tBFourthBraceMaterial.Location = new System.Drawing.Point(139, 88);
            this.tBFourthBraceMaterial.Name = "tBFourthBraceMaterial";
            this.tBFourthBraceMaterial.Size = new System.Drawing.Size(100, 20);
            this.tBFourthBraceMaterial.TabIndex = 9;
            // 
            // materialCatalog6
            // 
            this.structuresExtender.SetAttributeName(this.materialCatalog6, null);
            this.structuresExtender.SetAttributeTypeName(this.materialCatalog6, null);
            this.materialCatalog6.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.materialCatalog6, null);
            this.materialCatalog6.Location = new System.Drawing.Point(245, 87);
            this.materialCatalog6.Name = "materialCatalog6";
            this.materialCatalog6.SelectedMaterial = "";
            this.materialCatalog6.Size = new System.Drawing.Size(88, 22);
            this.materialCatalog6.TabIndex = 10;
            this.materialCatalog6.SelectClicked += new System.EventHandler(this.materialCatalog6_SelectClicked);
            this.materialCatalog6.SelectionDone += new System.EventHandler(this.materialCatalog6_SelectionDone);
            // 
            // tabPage4
            // 
            this.structuresExtender.SetAttributeName(this.tabPage4, null);
            this.structuresExtender.SetAttributeTypeName(this.tabPage4, null);
            this.structuresExtender.SetBindPropertyName(this.tabPage4, null);
            this.tabPage4.Controls.Add(this.tableLayoutPanel4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(523, 123);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Свойства раскосов 5 - 6";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.structuresExtender.SetAttributeName(this.tableLayoutPanel4, null);
            this.structuresExtender.SetAttributeTypeName(this.tableLayoutPanel4, null);
            this.tableLayoutPanel4.AutoSize = true;
            this.tableLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.structuresExtender.SetBindPropertyName(this.tableLayoutPanel4, null);
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.Controls.Add(this.tBFifthBraceMaterial, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.tBFifthBraceProfile, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.profileCatalog7, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.checkBox13, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.materialCatalog7, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.checkBox14, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.checkBox15, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.tBSixthBraceProfile, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.profileCatalog8, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.checkBox16, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.tBSixthBraceMaterial, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.materialCatalog8, 2, 3);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(6, 6);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(336, 112);
            this.tableLayoutPanel4.TabIndex = 13;
            // 
            // tBFifthBraceMaterial
            // 
            this.tBFifthBraceMaterial.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.tBFifthBraceMaterial, "FifthBraceMaterial");
            this.structuresExtender.SetAttributeTypeName(this.tBFifthBraceMaterial, "String");
            this.structuresExtender.SetBindPropertyName(this.tBFifthBraceMaterial, "Text");
            this.tBFifthBraceMaterial.Enabled = false;
            this.tBFifthBraceMaterial.Location = new System.Drawing.Point(139, 32);
            this.tBFifthBraceMaterial.Name = "tBFifthBraceMaterial";
            this.tBFifthBraceMaterial.Size = new System.Drawing.Size(100, 20);
            this.tBFifthBraceMaterial.TabIndex = 9;
            // 
            // tBFifthBraceProfile
            // 
            this.tBFifthBraceProfile.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.tBFifthBraceProfile, "FifthBraceProfile");
            this.structuresExtender.SetAttributeTypeName(this.tBFifthBraceProfile, "String");
            this.structuresExtender.SetBindPropertyName(this.tBFifthBraceProfile, "Text");
            this.tBFifthBraceProfile.Enabled = false;
            this.tBFifthBraceProfile.Location = new System.Drawing.Point(139, 4);
            this.tBFifthBraceProfile.Name = "tBFifthBraceProfile";
            this.tBFifthBraceProfile.Size = new System.Drawing.Size(100, 20);
            this.tBFifthBraceProfile.TabIndex = 9;
            // 
            // profileCatalog7
            // 
            this.profileCatalog7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.profileCatalog7, null);
            this.structuresExtender.SetAttributeTypeName(this.profileCatalog7, null);
            this.profileCatalog7.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.profileCatalog7, null);
            this.profileCatalog7.Location = new System.Drawing.Point(245, 3);
            this.profileCatalog7.Name = "profileCatalog7";
            this.profileCatalog7.SelectedProfile = "";
            this.profileCatalog7.Size = new System.Drawing.Size(88, 22);
            this.profileCatalog7.TabIndex = 8;
            this.profileCatalog7.SelectClicked += new System.EventHandler(this.profileCatalog7_SelectClicked);
            this.profileCatalog7.SelectionDone += new System.EventHandler(this.profileCatalog7_SelectionDone);
            // 
            // checkBox13
            // 
            this.checkBox13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.checkBox13, "FifthBraceMaterial");
            this.structuresExtender.SetAttributeTypeName(this.checkBox13, null);
            this.checkBox13.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox13, null);
            this.structuresExtender.SetIsFilter(this.checkBox13, true);
            this.checkBox13.Location = new System.Drawing.Point(3, 33);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(130, 17);
            this.checkBox13.TabIndex = 6;
            this.checkBox13.Text = "Материал раскоса 5";
            this.checkBox13.UseVisualStyleBackColor = true;
            // 
            // materialCatalog7
            // 
            this.structuresExtender.SetAttributeName(this.materialCatalog7, null);
            this.structuresExtender.SetAttributeTypeName(this.materialCatalog7, null);
            this.materialCatalog7.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.materialCatalog7, null);
            this.materialCatalog7.Location = new System.Drawing.Point(245, 31);
            this.materialCatalog7.Name = "materialCatalog7";
            this.materialCatalog7.SelectedMaterial = "";
            this.materialCatalog7.Size = new System.Drawing.Size(88, 22);
            this.materialCatalog7.TabIndex = 10;
            this.materialCatalog7.SelectClicked += new System.EventHandler(this.materialCatalog7_SelectClicked);
            this.materialCatalog7.SelectionDone += new System.EventHandler(this.materialCatalog7_SelectionDone);
            // 
            // checkBox14
            // 
            this.checkBox14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.checkBox14, "FifthBraceProfile");
            this.structuresExtender.SetAttributeTypeName(this.checkBox14, null);
            this.checkBox14.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox14, null);
            this.structuresExtender.SetIsFilter(this.checkBox14, true);
            this.checkBox14.Location = new System.Drawing.Point(3, 5);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(126, 17);
            this.checkBox14.TabIndex = 6;
            this.checkBox14.Text = "Профиль раскоса 5";
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // checkBox15
            // 
            this.checkBox15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.checkBox15, "SixthBraceProfile");
            this.structuresExtender.SetAttributeTypeName(this.checkBox15, null);
            this.checkBox15.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox15, null);
            this.structuresExtender.SetIsFilter(this.checkBox15, true);
            this.checkBox15.Location = new System.Drawing.Point(3, 61);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(126, 17);
            this.checkBox15.TabIndex = 6;
            this.checkBox15.Text = "Профиль раскоса 6";
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // tBSixthBraceProfile
            // 
            this.tBSixthBraceProfile.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.tBSixthBraceProfile, "SixthBraceProfile");
            this.structuresExtender.SetAttributeTypeName(this.tBSixthBraceProfile, "String");
            this.structuresExtender.SetBindPropertyName(this.tBSixthBraceProfile, "Text");
            this.tBSixthBraceProfile.Enabled = false;
            this.tBSixthBraceProfile.Location = new System.Drawing.Point(139, 60);
            this.tBSixthBraceProfile.Name = "tBSixthBraceProfile";
            this.tBSixthBraceProfile.Size = new System.Drawing.Size(100, 20);
            this.tBSixthBraceProfile.TabIndex = 9;
            // 
            // profileCatalog8
            // 
            this.profileCatalog8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.profileCatalog8, null);
            this.structuresExtender.SetAttributeTypeName(this.profileCatalog8, null);
            this.profileCatalog8.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.profileCatalog8, null);
            this.profileCatalog8.Location = new System.Drawing.Point(245, 59);
            this.profileCatalog8.Name = "profileCatalog8";
            this.profileCatalog8.SelectedProfile = "";
            this.profileCatalog8.Size = new System.Drawing.Size(88, 22);
            this.profileCatalog8.TabIndex = 8;
            this.profileCatalog8.SelectClicked += new System.EventHandler(this.profilecatalog8_selectclicked);
            this.profileCatalog8.SelectionDone += new System.EventHandler(this.profileCatalog8_SelectionDone);
            // 
            // checkBox16
            // 
            this.checkBox16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.checkBox16, "SixthBraceMaterial");
            this.structuresExtender.SetAttributeTypeName(this.checkBox16, null);
            this.checkBox16.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.checkBox16, null);
            this.structuresExtender.SetIsFilter(this.checkBox16, true);
            this.checkBox16.Location = new System.Drawing.Point(3, 89);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(130, 17);
            this.checkBox16.TabIndex = 6;
            this.checkBox16.Text = "Материал раскоса 6";
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // tBSixthBraceMaterial
            // 
            this.tBSixthBraceMaterial.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.structuresExtender.SetAttributeName(this.tBSixthBraceMaterial, "SixthBraceMaterial");
            this.structuresExtender.SetAttributeTypeName(this.tBSixthBraceMaterial, "String");
            this.structuresExtender.SetBindPropertyName(this.tBSixthBraceMaterial, "Text");
            this.tBSixthBraceMaterial.Enabled = false;
            this.tBSixthBraceMaterial.Location = new System.Drawing.Point(139, 88);
            this.tBSixthBraceMaterial.Name = "tBSixthBraceMaterial";
            this.tBSixthBraceMaterial.Size = new System.Drawing.Size(100, 20);
            this.tBSixthBraceMaterial.TabIndex = 9;
            // 
            // materialCatalog8
            // 
            this.structuresExtender.SetAttributeName(this.materialCatalog8, null);
            this.structuresExtender.SetAttributeTypeName(this.materialCatalog8, null);
            this.materialCatalog8.BackColor = System.Drawing.Color.Transparent;
            this.structuresExtender.SetBindPropertyName(this.materialCatalog8, null);
            this.materialCatalog8.Location = new System.Drawing.Point(245, 87);
            this.materialCatalog8.Name = "materialCatalog8";
            this.materialCatalog8.SelectedMaterial = "";
            this.materialCatalog8.Size = new System.Drawing.Size(88, 22);
            this.materialCatalog8.TabIndex = 10;
            this.materialCatalog8.SelectClicked += new System.EventHandler(this.materialCatalog8_SelectClicked);
            this.materialCatalog8.SelectionDone += new System.EventHandler(this.materialCatalog8_SelectionDone);
            // 
            // comboBox1
            // 
            this.structuresExtender.SetAttributeName(this.comboBox1, "LoadingIndex");
            this.structuresExtender.SetAttributeTypeName(this.comboBox1, "Integer");
            this.structuresExtender.SetBindPropertyName(this.comboBox1, "SelectedIndex");
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "1260",
            "1410",
            "1560",
            "1710",
            "1860",
            "2010",
            "2160",
            "Не задано"});
            this.comboBox1.Location = new System.Drawing.Point(166, 53);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 12;
            // 
            // label1
            // 
            this.structuresExtender.SetAttributeName(this.label1, null);
            this.structuresExtender.SetAttributeTypeName(this.label1, null);
            this.label1.AutoSize = true;
            this.structuresExtender.SetBindPropertyName(this.label1, null);
            this.label1.Location = new System.Drawing.Point(9, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Нагрузка на ферму, кг/п.м.:";
            // 
            // TrussPluginForm
            // 
            this.structuresExtender.SetAttributeName(this, null);
            this.structuresExtender.SetAttributeTypeName(this, null);
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.structuresExtender.SetBindPropertyName(this, null);
            this.ClientSize = new System.Drawing.Size(538, 258);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.saveLoad1);
            this.Controls.Add(this.okApplyModifyGetOnOffCancel1);
            this.Name = "TrussPluginForm";
            this.Text = "Ферма Ф18";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Tekla.Structures.Dialog.UIControls.OkApplyModifyGetOnOffCancel okApplyModifyGetOnOffCancel1;
        private Tekla.Structures.Dialog.UIControls.SaveLoad saveLoad1;
        private Tekla.Structures.Dialog.UIControls.ProfileCatalog profileCatalog1;
        private System.Windows.Forms.TextBox tBLowerBraceProfile;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox tBLowerBraceMaterial;
        private System.Windows.Forms.CheckBox checkBox6;
        private Tekla.Structures.Dialog.UIControls.MaterialCatalog materialCatalog1;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.TextBox tBUpperBraceProfile;
        private Tekla.Structures.Dialog.UIControls.ProfileCatalog profileCatalog2;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.TextBox tBUpperBraceMaterial;
        private Tekla.Structures.Dialog.UIControls.MaterialCatalog materialCatalog2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox tBFirstBraceMaterial;
        private System.Windows.Forms.TextBox tBFirstBraceProfile;
        private Tekla.Structures.Dialog.UIControls.ProfileCatalog profileCatalog3;
        private System.Windows.Forms.CheckBox checkBox1;
        private Tekla.Structures.Dialog.UIControls.MaterialCatalog materialCatalog3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.TextBox tBSecondBraceProfile;
        private Tekla.Structures.Dialog.UIControls.ProfileCatalog profileCatalog4;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.TextBox tBSecondBraceMaterial;
        private Tekla.Structures.Dialog.UIControls.MaterialCatalog materialCatalog4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TextBox tBThirdBraceMaterial;
        private System.Windows.Forms.TextBox tBThirdBraceProfile;
        private Tekla.Structures.Dialog.UIControls.ProfileCatalog profileCatalog5;
        private System.Windows.Forms.CheckBox checkBox9;
        private Tekla.Structures.Dialog.UIControls.MaterialCatalog materialCatalog5;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.TextBox tBFourthBraceProfile;
        private Tekla.Structures.Dialog.UIControls.ProfileCatalog profileCatalog6;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.TextBox tBFourthBraceMaterial;
        private Tekla.Structures.Dialog.UIControls.MaterialCatalog materialCatalog6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TextBox tBFifthBraceMaterial;
        private System.Windows.Forms.TextBox tBFifthBraceProfile;
        private Tekla.Structures.Dialog.UIControls.ProfileCatalog profileCatalog7;
        private System.Windows.Forms.CheckBox checkBox13;
        private Tekla.Structures.Dialog.UIControls.MaterialCatalog materialCatalog7;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.TextBox tBSixthBraceProfile;
        private Tekla.Structures.Dialog.UIControls.ProfileCatalog profileCatalog8;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.TextBox tBSixthBraceMaterial;
        private Tekla.Structures.Dialog.UIControls.MaterialCatalog materialCatalog8;
    }
}

