﻿using System;

using Tekla.Structures.Dialog;

namespace FerroBuilding.Truss
{
    public partial class TrussPluginForm : PluginFormBase
    {
        private const string DEFAULT_PROFILE = "SQR100";
        private const string DEFAULT_MATERIAL = "Steel_Undefined";

        public TrussPluginForm()
        {
            this.InitializeComponent();
        }

        //Use default values for plugins in models without a standard attribute file
        protected override string LoadValuesPath(string fileName)
        {
            this.SetAttributeValue(this.tBLowerBraceProfile, DEFAULT_PROFILE);
            this.SetAttributeValue(this.tBLowerBraceMaterial, DEFAULT_MATERIAL);

            this.SetAttributeValue(this.tBUpperBraceProfile, DEFAULT_PROFILE);
            this.SetAttributeValue(this.tBUpperBraceMaterial, DEFAULT_MATERIAL);

            this.SetAttributeValue(this.tBFirstBraceProfile, DEFAULT_PROFILE);
            this.SetAttributeValue(this.tBFirstBraceMaterial, DEFAULT_MATERIAL);

            this.SetAttributeValue(this.tBSecondBraceProfile, DEFAULT_PROFILE);
            this.SetAttributeValue(this.tBSecondBraceMaterial, DEFAULT_MATERIAL);

            this.SetAttributeValue(this.tBThirdBraceProfile, DEFAULT_PROFILE);
            this.SetAttributeValue(this.tBThirdBraceMaterial, DEFAULT_MATERIAL);

            this.SetAttributeValue(this.tBFourthBraceProfile, DEFAULT_PROFILE);
            this.SetAttributeValue(this.tBFourthBraceMaterial, DEFAULT_MATERIAL);

            this.SetAttributeValue(this.tBFifthBraceProfile, DEFAULT_PROFILE);
            this.SetAttributeValue(this.tBFifthBraceMaterial, DEFAULT_MATERIAL);

            this.SetAttributeValue(this.tBSixthBraceProfile, DEFAULT_PROFILE);
            this.SetAttributeValue(this.tBSixthBraceMaterial, DEFAULT_MATERIAL);


            string Result = base.LoadValuesPath(fileName);
            this.Apply();
            return Result;
        }

        //Remember to assign these events to the buttons.
        private void okApplyModifyGetOnOffCancel1_ApplyClicked(object sender, EventArgs e)
        {
            this.Apply();
        }

        private void okApplyModifyGetOnOffCancel1_CancelClicked(object sender, EventArgs e)
        {
            this.Close();
        }

        private void okApplyModifyGetOnOffCancel1_GetClicked(object sender, EventArgs e)
        {
            this.Get();
        }

        private void okApplyModifyGetOnOffCancel1_ModifyClicked(object sender, EventArgs e)
        {
            this.Modify();
        }

        private void okApplyModifyGetOnOffCancel1_OkClicked(object sender, EventArgs e)
        {
            this.Apply();
            this.Close();
        }

        private void okApplyModifyGetOnOffCancel1_OnOffClicked(object sender, EventArgs e)
        {
            this.ToggleSelection();
        }

        private void profileCatalog1_SelectClicked(object sender, EventArgs e) => this.profileCatalog1.SelectedProfile = this.tBLowerBraceProfile.Text;

        private void profileCatalog1_SelectionDone(object sender, EventArgs e) => this.SetAttributeValue(this.tBLowerBraceProfile, this.profileCatalog1.SelectedProfile);

        private void profileCatalog2_SelectClicked(object sender, EventArgs e) => this.profileCatalog2.SelectedProfile = this.tBUpperBraceProfile.Text;

        private void profileCatalog2_SelectionDone(object sender, EventArgs e) => this.SetAttributeValue(this.tBUpperBraceProfile, this.profileCatalog2.SelectedProfile);

        private void profileCatalog3_SelectClicked(object sender, EventArgs e) => this.profileCatalog3.SelectedProfile = this.tBFirstBraceProfile.Text;

        private void profileCatalog3_SelectionDone(object sender, EventArgs e) => this.SetAttributeValue(this.tBFirstBraceProfile, this.profileCatalog3.SelectedProfile);

        private void profileCatalog4_SelectClicked(object sender, EventArgs e) => this.profileCatalog4.SelectedProfile = this.tBSecondBraceProfile.Text;

        private void profileCatalog4_SelectionDone(object sender, EventArgs e) => this.SetAttributeValue(this.tBSecondBraceProfile, this.profileCatalog4.SelectedProfile);

        private void profileCatalog5_SelectClicked(object sender, EventArgs e) => this.profileCatalog5.SelectedProfile = this.tBThirdBraceProfile.Text;

        private void profileCatalog5_SelectionDone(object sender, EventArgs e) => this.SetAttributeValue(this.tBThirdBraceProfile, this.profileCatalog5.SelectedProfile);

        private void profileCatalog6_SelectClicked(object sender, EventArgs e) => this.profileCatalog6.SelectedProfile = this.tBFourthBraceProfile.Text;

        private void profileCatalog6_SelectionDone(object sender, EventArgs e) => this.SetAttributeValue(this.tBFourthBraceProfile, this.profileCatalog6.SelectedProfile);

        private void profileCatalog7_SelectClicked(object sender, EventArgs e) => this.profileCatalog7.SelectedProfile = this.tBFifthBraceProfile.Text;

        private void profileCatalog7_SelectionDone(object sender, EventArgs e) => this.SetAttributeValue(this.tBFifthBraceProfile, this.profileCatalog7.SelectedProfile);

        private void profilecatalog8_selectclicked(object sender, EventArgs e) => this.profileCatalog8.SelectedProfile = this.tBSixthBraceProfile.Text;

        private void profileCatalog8_SelectionDone(object sender, EventArgs e) => this.SetAttributeValue(this.tBSixthBraceProfile, this.profileCatalog8.SelectedProfile);

        private void materialCatalog1_SelectClicked(object sender, EventArgs e) => this.materialCatalog1.SelectedMaterial = this.tBLowerBraceMaterial.Text;

        private void materialCatalog1_SelectionDone(object sender, EventArgs e) => this.SetAttributeValue(this.tBLowerBraceMaterial, this.materialCatalog1.SelectedMaterial);

        private void materialCatalog2_SelectClicked(object sender, EventArgs e) => this.materialCatalog2.SelectedMaterial = this.tBLowerBraceMaterial.Text;

        private void materialCatalog2_SelectionDone(object sender, EventArgs e) => this.SetAttributeValue(this.tBUpperBraceMaterial, this.materialCatalog2.SelectedMaterial);

        private void materialCatalog3_SelectClicked(object sender, EventArgs e) => this.materialCatalog3.SelectedMaterial = this.tBFirstBraceMaterial.Text;

        private void materialCatalog3_SelectionDone(object sender, EventArgs e) => this.SetAttributeValue(this.tBFirstBraceMaterial, this.materialCatalog3.SelectedMaterial);

        private void materialCatalog4_SelectClicked(object sender, EventArgs e) => this.materialCatalog4.SelectedMaterial = this.tBSecondBraceMaterial.Text;

        private void materialCatalog4_SelectionDone(object sender, EventArgs e) => this.SetAttributeValue(this.tBSecondBraceMaterial, this.materialCatalog4.SelectedMaterial);

        private void materialCatalog5_SelectClicked(object sender, EventArgs e) => this.materialCatalog5.SelectedMaterial = this.tBThirdBraceMaterial.Text;

        private void materialCatalog5_SelectionDone(object sender, EventArgs e) => this.SetAttributeValue(this.tBThirdBraceMaterial, this.materialCatalog5.SelectedMaterial);

        private void materialCatalog6_SelectClicked(object sender, EventArgs e) => this.materialCatalog6.SelectedMaterial = this.tBFourthBraceMaterial.Text;

        private void materialCatalog6_SelectionDone(object sender, EventArgs e) => this.SetAttributeValue(this.tBFourthBraceMaterial, this.materialCatalog6.SelectedMaterial);

        private void materialCatalog7_SelectClicked(object sender, EventArgs e) => this.materialCatalog7.SelectedMaterial = this.tBFifthBraceMaterial.Text;

        private void materialCatalog7_SelectionDone(object sender, EventArgs e) => this.SetAttributeValue(this.tBFifthBraceMaterial, this.materialCatalog7.SelectedMaterial);

        private void materialCatalog8_SelectClicked(object sender, EventArgs e) => this.materialCatalog8.SelectedMaterial = this.tBSixthBraceMaterial.Text;

        private void materialCatalog8_SelectionDone(object sender, EventArgs e) => this.SetAttributeValue(this.tBSixthBraceMaterial, this.materialCatalog8.SelectedMaterial);
    }
}
